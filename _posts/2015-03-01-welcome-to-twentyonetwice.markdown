---
layout: post
title:  "Willkommen bei 21x2!"
date:   2015-03-01 13:30:10
categories: rails
---
Hey, da heute der 01.03. und die Webseite endlich fertig geworden ist, sollte ich gleich mal einen Post verfassen, damit zumindest mal etwas im Blog veröffentlich wurde.

Die Webseite hier ist nicht dynamisch, sondern eine rein statische Webseite, die mit [Jekyll][jekyll] und [Foundation][foundation] erstellt wurde.

Der erste richtige Post wird dann über Rails-Applikationen mit [ArangoDB][arangodb] berichten

[jekyll]:     http://jekyllrb.com
[foundation]: http://http://foundation.zurb.com/
[arangodb]: 	https://www.arangodb.com/

