---
layout: post
title:  "Rails mit ArangoDB"
date:   2015-03-01 17:00:10
categories: rails
---
#### ArangoDB

[ArangoDB] ist eine Multi-Model NoSQL Datenbank, welche die Datenmodelle mit Graphen, key-value-Paaren und documents
darstellen kann. Statt Daten wie in herkömmlichen relationalen Datenbanken in Tabellen zu speichern, werden diese
hier in collections abgelegt. Die Spaltenbezeichnung aus RDBMSs heißen bei ArangoDB attributes und die einzelnen
Tabellenzeilen heißen dann documents. Diese Vorab-Erklärung soll nur dazu dienen, im späteren Verlauf eine
gemeinsames Vokabular zu haben.

[ArangoDB] läuft auf allen gängigen Systemen. Näheres zur Installation auf Eurem System findet Ihr im [Guide zur
Installation][ArangoDB-inst].

[ArangoDB]: 	https://www.arangodb.com/
[ArangoDB-inst]: 	https://docs.arangodb.com/Installing/README.html
