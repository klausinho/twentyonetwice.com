# README

You can use this repo to install a jekyll based website.

Make sure you have ruby and bundler installed.

Clone this repo using the following command:

`git clone git@bitbucket.org:klausinho/twentyonetwice.com.git your-website-folder`

Edit 'your-website-folder' to whatever you want.

After cloning this repo, be sure to remove the hidden .git folder to create your own repo.

Switch to your freshly created folder (your-website-folder) and run

`bundle install`

After that edit the _config.yml with your own values.

Run `compass watch` in a terminal session.

Run `jekyll build` to create your _site folder with all jekyll generated files or run `jekyll serve -w` to run the
server and watch all changes on the fly.

Edit the scss-files (scss-folder) and html-files (_includes-folder) to your preferences.
