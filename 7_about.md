---
layout: page
title: Über diesen Blog
permalink: /about/
---

Nach langen endlosen Anläufen habe ich jetzt endlich mal einen Blog aufgesetzt. Eigentlich war das mal mehr für mich als
Nachschlagewerk gedacht, weil es irgendwann nervt, wenn man im Laufe der Zeit feststellt, dass man aml wieder zum x-mal
nach einer Lösung für ein und dasselbe Problem gegooglet hat. Naja, und wenn man sowieso schon eine Webseite zusammenschraubt,
dann kann man das auch gleich öffentlich machen. So hat eventuell der eine oder andere Mensch auch noch was davon.

Wie ich rechts schon geschrieben habe, befasse ich mich mit dem Thema Webseiten schon seit über 20 Jahren. Wie man an dieser
Seite hier sieht, bin ich nicht der Designer vor dem Herrn, aber da dies hier ja ein informativer Blog sein soll, ist das wurscht.

Ich habe zu Beginn tatsächlich noch mit - ja, ich oute mich jetzt - Frontpage gearbeitet und bin dann irgendwann so Mitte PHP 2
auf dynamische Inhalte umgestiegen. Bis vor ca. 3 Jahren war ich felsenfest der Meinung, dass PHP für jeden Programmier-Neuling
DIE Sprache schlechthin ist, weil die Lernkurve hier, wenn man von conditions und loops und iterations noch nie was gehört hat,
relativ steil ist.

Mittlerweile hat Ruby da PHP meiner Meinung nach ganz klar den Rang abgelaufen. Wenn man als Framework dann auch noch
Ruby on Rails einsetzt, hat man sich eine Umgebung geschaffen, die durch ihre Intuitivität und vor alle Lesbarkeit des Codes
einfach unschlagbar ist.

Ruby bzw. Ruby on Rails hat nur einen kleinen Nachteil: Es gibt kaum Blogs oder Tutorials in Deutsch im Netz! Ja, ich weiß,
wer codet, der kann normalerweise auch Englisch. Die Frage ist halt oft nur, wie gut sind die Englischkenntnisse, vor allem,
wenn man sich in ein Gebiet einarbeiten will, von dem man absolut keine Ahnung hat!

Deshalb habe ich mich entschlossen, diesen Blog hier in Deutsch zu verfassen, auch wenn da die Community sicherlich kleiner ist.
Abgesehen davon nehme ich für mich auch nicht in Anspruch, das Rad auf einmal als"rund" zu erfinden. Ich bin überzeugt davon,
dass alles, was hier steht, irgendwo im Netz schon mal auf Englisch niedergeschrieben wurde.

Eines noch zum Thema "Blog in Deutsch": Es gibt gerade in der IT massig Begriffe auf englisch, bei denen es schlicht
keinen Sinn macht, diese einzudeutschen. Hier geht es vor allem darum, die Erklärungen "drumherum" in deutscher
Sprache weiterzugeben. Ein Link ist nun mal ein Link, auch wenn sich das mit Verknüpfung oder Verbindung übersetzen
lässt.
